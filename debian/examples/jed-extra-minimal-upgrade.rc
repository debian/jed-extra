%                               -*- slang -*-
%                               
% example user initialization for jed-extra
% =========================================
%   
% To get most out of the add-on modes in jed-extra, some activation code
% is needed. As this slows down the startup and might conflict with private
% extensions this activation should be done in the users jed.rc.
%
% You are invited to insert this file into your jed.rc and modify
% according to your needs.
% 
% Many modes require additional steps to be usable: see the documentation
% at the top of the source file. (Help>Grep_Definition can help to find the
% source of a library function.)

% jed-extra provides one more emulation mode (in addition to jed-common)
% if (BATCH == 0)
% {
%  require("vi");            % vi emulation
% }

% The jed-extra library dir
$1 = "/usr/share/jed/jed-extra/";

% Add and initialize library directories
% --------------------------------------

% The default setup in /etc/jed.d/50jed-extra.sl adds library directories
% for normal modes, utilities, and drop-in modes.

% Utilities (required by the other modes)
%   Initialization adds autoloads for utility functions:
%    - slows down startup
%    + lets private modes use the util functions without need for autoloads,
%      some "extra" modes rely on these autoloads as well
%    + lets Help>Apropos find util functions right from the start
% () = evalfile($1+"utils/ini.sl"); 

% "Normal" modes
%   Initialization is already done in the default config (but not in minimal)
%   (check /etc/jed.d/50jed-extra*.sl)
() = evalfile($1+"ini.sl"); 

% Drop-In Modes (recent, help, man, ...) need to be prepended
%   Initialization is low key (autolaods for additional functions)
() = evalfile($1+"drop-in/ini.sl"); 


% Experimental and exotic modes
% '''''''''''''''''''''''''''''

%   Recommended use is to copy or link interesting modes
%   to /usr/local/share/jed/lib/ or ~/.jed/lib 
%   and use make_ini() and add_libdir() for activation (see below)
%   
%   Another option is to require() or autoload() with the path
%   relative to the jed-library-path, (you can copy autoloads from
%   /usr/share/jed/jed-extra/extra/ini.sl)  
%   
%   However, this approach will fail if the mode depends on another "extra"
%   mode and expects it in the jed-library-path.
%   
%   e.g.
%
% % ffap.sl: find file at point
% "add_list_element", "extra/ffap.sl";
% "ffap_set_info", "extra/ffap.sl";
% "ffap", "extra/ffap.sl";
% _autoload(3);
% 
% "add_list_element";
% "ffap_set_info";
% "ffap";
% _add_completion(3);
%
% % services.sl: uri_hooks for some common URI schemes.
% require("services", "extra/services.sl");

% Full activation of "extra" modes:
%   Uncomment at your own risc
% append_libdir($1 + "extra/", 0);  % append but do not initialize
% append_libdir($1 + "extra/", 1);  % append and initialize
% 
%   Alternatively, add documentation for "extra functions" to the online help
Jed_Doc_Files += "," + $1+"extra/libfuns.txt";

% "Manual" initialization of stuff not handled by the ini.sl files
% ----------------------------------------------------------------

% some examples for additional code needed to make full use of the jed-extra
% modes:

% call extension-dependend modes
add_mode_for_extension ("css1", "css");
add_mode_for_extension ("css1", "css1");
add_mode_for_extension ("gnuplot", "gnuplot");  % gnuplot plotting program
add_mode_for_extension ("gnuplot", "gp");
add_mode_for_extension ("mupad", "mu"); % mode for mupad files
add_mode_for_extension ("sql", "sql");
foreach (["man", "1", "2", "3", "4", "5", "6", "7", "8"]) {
  $2 = ();
  add_mode_for_extension ("manedit", $2);
}

% activate extension modes
require("numbuf");       % number buffers for fast switching
require("navigate");     % history feature (recent buffers)
require("ispell_init");  % improved ispell|aspell support
% require("cuamark");      % use CUA style kind of selecting regions


% Jed_Home_Directory and private extensions
% -----------------------------------------

% User extensions
%   Debian sets Jed_Home_Directory to ~/.jed/ if this dir exists
add_libdir(path_concat(Jed_Home_Directory, "lib"), 1);
% Jed_Highlight_Cache_Dir = path_concat(Jed_Home_Directory, "lib");

% Site wide extensions
add_libdir("/usr/local/share/jed/lib/", 1);

