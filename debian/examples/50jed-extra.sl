% 50jed-extra.sl: Initialization of jed-extra package

% Copyright (c) 2005 Debian JED Group
%  Released under the terms of the GNU General Public License (v. 2 or later)

% Location of the jed-extra package's library dir
private variable extra_lib = "/usr/share/jed/jed-extra/";

% if jed-extra is removed (not purged), this file (and possibly the
% jed-extra library dir) exist, but nothing else it expects --> abort
$1 = path_concat(extra_lib, "libdir.slc");
#if (file_status($1) != 1)
#stop
#endif

% Add (and initialize) library directories
% ----------------------------------------

() = evalfile($1);

% Utilities (required by the other modes)
% append_libdir(extra_lib + "utils/"), 1);           % append and initialize
append_libdir(path_concat(extra_lib, "utils/"), 0);  % do not initialize

% "Normal" modes
append_libdir(extra_lib, 1);   	    	   	     % append and initialize
% append_libdir(extra_lib, 0);             	     % do not initialize

% Drop-In Modes (recent, help, ...) 
% prepend to overwrite originals (comment out to keep the original behaviour)
add_libdir(path_concat(extra_lib, "drop-in/"), 1);   % prepend and initialize
% add_libdir(path_concat(extra_lib, "drop-in/"), 0); % do not initialize

% Experimental and exotic modes
% (uncomment to initalize, maybe better on a per user basis in ~/jed.rc)
% append_libdir(path_concat(extra_lib, "extra/"), 1);  % append and initialize
% append_libdir(path_concat(extra_lib, "extra/"), 0);  % do not initialize


% "Manual" initialization of stuff not handled by the ini.sl files
% ----------------------------------------------------------------

% add some autoloads
autoload("push_defaults", "sl_utils");    % needed by ispell_init.sl, complete, occur, ...
autoload("string_nth_match", "strutils"); % needed by hyperman.sl
autoload("get_keystring", "strutils");    % needed by snake.sl
% alternatively evaluate the utils/ini.sl file (or set the "initialize"
% argument to 1 in append_libdir(path_concat(extra_lib, "utils/"), 1) above)
% () = evalfile("utils/ini.sl");          % autoloads for all utility functions

% Set modes for known filename extensions
add_mode_for_extension ("css1", "css");
add_mode_for_extension ("css1", "css1");
add_mode_for_extension ("gnuplot", "gnuplot");  % gnuplot plotting program
add_mode_for_extension ("gnuplot", "gp");
foreach $1 (["man", "1", "2", "3", "4", "5", "6", "7", "8"]) 
   add_mode_for_extension ("manedit", $1);
add_mode_for_extension ("mupad", "mu"); % mode for mupad files
add_mode_for_extension ("ruby", "rb");
add_mode_for_extension ("sql", "sql");

% Update the System>Ispell menu entry
autoload("ispell_change_dictionary_callback", "ispell_common");
static define ispell_load_popup_hook (menubar)
{
   variable menu = "Global.S&ystem";
   menu_delete_item (menu + ".&Ispell");
   menu_append_popup (menu, "&Ispell");
   menu = "Global.S&ystem.&Ispell";
   menu_set_select_popup_callback(menu, &ispell_change_dictionary_callback);
}
append_to_hook ("load_popup_hooks", &ispell_load_popup_hook);
% alternatively:
% require("ispell_init");

% Globally activate extensions
% ----------------------------

% If there are several JED users on the machine, better do this activation in
% the user's jed.rc files, as there is no way a "normal" user can revert
% the choices short of disabling all of the Debian configuration  with
% `jed --skip-debian-startup`.

% require("numbuf");       % number buffers for fast switching
% require("navigate");     % history feature (recent buffers)
% require("ispell_init");  % ispell|aspell support

% require("cuamark");      % use CUA style kind of selecting regions

% Printing
% --------

% there are 2 alternative functions to send the current buffer to a printer
%
% autoload("print_buffer", "print");   % simple, sends pure ASCII
% autoload("print_buffer", "apsmode"); % uses a2ps, needs to be configured
%
% define print_popup_hook (menubar)
% {
%   menu_insert_separator (6, "Global.&File");
%   menu_insert_item (7, "Global.&File", "&Print Buffer", "print_buffer");
% }
% append_to_hook ("load_popup_hooks", &print_popup_hook);
