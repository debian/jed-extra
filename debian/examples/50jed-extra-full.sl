% 50jed-extra.sl: Initialization of jed-extra package
%
% Copyright (c) 2005 Debian JED Group
%  Released under the terms of the GNU General Public License (v. 2 or later)
%
% The full version of the initalization file does an "almost complete"
% activation of the jed-extra modes.
% 
% Copy to /etc/jed.d/50jed-extra.sl if you are sure all users of your site want
% extensive support `out of the box'.
% 
% Beware: evaluation of initialization files cannot be reverted by normal
% users by other means than completely disabling the Debian startup scheme
% with the command line option --skip-debian-startup. If this frightens you
% (or your users), go for the "minimal" version of 50jed-extra.

% Location of the jed-extra package's library dir
$1 = "/usr/share/jed/jed-extra/";

% if jed-extra is removed (not purged), this file (and possibly the
% jed-extra library dir) exist, but nothing else it expects --> abort
$2 = path_concat($1, "libdir.sl");
#if (file_status($2) != 1)
#stop
#endif

% Add (and initialize) library directories
% ----------------------------------------

() = evalfile($2);

% Utilities (required by the other modes)
%   Initialization adds autoloads for utility functions:
%    - slows down startup
%    + lets private modes use the util functions without need for autoloads
%    + lets Help>Apropos find util functions right from the start
%    + some modes from jed-extra/extra depend on this autoloads
append_libdir($1 + "utils/", 1);    % append and initialize 
% append_libdir($1 + "utils/", 0);  % append but do not initialize

% "Normal" modes
%   Initialization is low key (autolaods, some Menu entries)
append_libdir($1, 1);   	    % append and initialize 
% append_libdir($1, 0);             % append but do not initialize

% Drop-In Modes (recent, help, man, ...) need to be prepended
%   Comment if you want to keep the original behaviour
%   Initialization is low key (autolaods for additional functions)
add_libdir($1 + "drop-in/", 1);     % append and initialize 
% add_libdir($1 + "drop-in/", 0);   % append but do not initialize

% Experimental and exotic modes
%   Uncomment at your own risc (maybe better per user in ~/jed.rc) 
%   The recommended use is to, copy or link interesting modes
%   to /usr/local/share/jed/lib/ or ~/.jed/lib and user make_ini() and
%   add_libdir() for activation (in ~/jed.rc).
% append_libdir($1 + "extra/", 0);  % append but do not initialize
% append_libdir($1 + "extra/", 1);  % append and initialize 


% "Manual" initialization of stuff not handled by the ini.sl files
% ----------------------------------------------------------------

% add some autoloads (not needed if utils/ini.sl is evaluated, e.g. with 
%     	   	      append_libdir($1 + "utils/", 1);)
% autoload("push_defaults", "sl_utils");    % needed by ispell_init.sl, complete, occur, ...
% autoload("string_nth_match", "strutils"); % needed by hyperman.sl
% autoload("get_keystring", "strutils");    % needed by snake.sl

% call extension-dependend modes
add_mode_for_extension ("css1", "css");
add_mode_for_extension ("css1", "css1");
add_mode_for_extension ("gnuplot", "gnuplot");  % gnuplot plotting program
add_mode_for_extension ("gnuplot", "gp");
add_mode_for_extension ("mupad", "mu"); % mode for mupad files
foreach (["man", "1", "2", "3", "4", "5", "6", "7", "8"]) {
  $2 = ();
  add_mode_for_extension ("manedit", $2);
}
add_mode_for_extension ("sql", "sql");

% Globally activate extensions
% ----------------------------

% If there are more JED users on the machine, better do this activation in
% the user's jed.rc files, as there is no way a "normal" user can revert
% the choices short of disabling all of the Debian configuration  with
% `jed --skip-debian-startup`.

require("numbuf");       % number buffers for fast switching
require("navigate");     % history feature (recent buffers)
require("ispell_init");  % ispell|aspell support
% require("cuamark");      % use CUA style kind of selecting regions

% Printing
% --------
 
% there are 2 alternative functions to send the current buffer to a printer
% The user can easily override the choice in his|her jed.rc.
% 
autoload("print_buffer", "print");   % simple, sends pure ASCII
% autoload("print_buffer", "apsmode"); % uses a2ps, needs to be configured

define print_popup_hook (menubar)
{
  menu_insert_separator (6, "Global.&File");
  menu_insert_item (7, "Global.&File", "&Print Buffer", "print_buffer");
}
append_to_hook ("load_popup_hooks", &print_popup_hook);
